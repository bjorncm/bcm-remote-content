<?php
/*
Plugin Name: Remote Content
Plugin URI: http://www.yourstyledesign.nl/
Description: Include the content of another Wordpress site via the Wordpress REST API
Version: 2.0.3
Author: Bjorn Manintveld
Author URI: http://www.yourstyledesign.nl/
*/

    function sc_include($atts)
    {
        extract(shortcode_atts(array(
            'url' => ' ',
        ), $atts));
        $a=file_get_contents($url);
        return $a;
    }
    add_shortcode('include', 'sc_include');

    function ysd_remote_content_shortcode($atts)
    {
        extract(
            shortcode_atts(
            array(
                'title' => 0,
                'url' => 0,
                'id' => 0,
            ),
            $atts
        )
        );
        $output = "";
        if (empty($id)) {
            return "Geen Pagina ID ingesteld.";
        } else {
            $json = ysd_remote_content_feeder($id, $url);
            if (!empty($title)) {
                $title = $json->title;
                $output.= "<h1 class=\"title\">".$title->rendered."</h1>";
            }
            $content = $json->content;
            $output.= $content->rendered;
            return $output;
        }
    }
    add_shortcode('remote_content', 'ysd_remote_content_shortcode');

    // Cache JSON feed and parse content
    function ysd_remote_content_feeder($id, $url)
    {
        $uploads_dir = wp_upload_dir();
        $uploads_dir = $uploads_dir['basedir'];
        $dir = $uploads_dir . '/remote-content/';
        if (!file_exists($dir) && !is_dir($dir)) {
            mkdir($dir);
        }
        if (isset($id) && !empty($id)) {
            $cache_time = 3600 * 12; //12 hours
            $cache_file = $dir.'pages-'.$id.'.json';
            $timedif = @(time() - filemtime($cache_file));
            $json_source_url = $url.'/wp-json/wp/v2/pages/'.$id;

            // remove white space(s) and/or space(s) from connector code
            $json_source_url = str_replace(' ', '', $json_source_url);
            $json_source_url = preg_replace('/\s+/', '', $json_source_url);
            
            // remove trailing slash at the end of the url
            if (substr($json_source_url, -1) == '/') {
                $json_source_url = substr($json_source_url, 0, -1);
            }

            // renew the cache file if needed
            if (file_exists($cache_file) && $timedif < $cache_time) {
                $string = file_get_contents($cache_file);
            } else {
                // set a time-out (5 sec) on fetch feed
                $context = array('http' => array(
                    'timeout'       => 5,
                ) );
                $pure_context = stream_context_create($context);
                //$string = file_get_contents($json_source_url, false, $pure_context);
                $string = file_get_contents($json_source_url);
                
                // don't override cache file if the new data is empty
                if (empty($string)) {
                    $string = file_get_contents($cache_file);
                }

                // override cache file if writable
                if ($f = @fopen($cache_file, 'w')) {
                    fwrite($f, $string, strlen($string));
                    fclose($f);
                }
            }
        }

        return json_decode($string);
    }
