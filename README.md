# Remote Content

## Changelog

### v2.0.3 (2018-12-20)
* Bugfixes

### v2.0.2 (2018-11-08)
* Bugfixes

### v2.0.1 (2018-07-16)
* Bugfixes

### v2.0.0 (2018-07-05)
* Complete rewrite, now using build in REST API functionality from Wordpress

### v1.0.0 (2012-10-22)
* Initial Release
